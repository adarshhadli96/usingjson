﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JsonApp.Dtos
{
    public class JsonObjMapping
    {
        public string? status { get; set; }
        public string? copyright { get; set; }
        public int? num_results { get; set; }
        public DateTime? last_modified { get; set; }

    }

    public class book_details
    {
        public string? title { get; set; }
        public string? description { get; set; }
        public string? contributor { get; set; }
        public string? author { get; set; }
        public string? contributor_note { get; set; }
        public int price { get; set; }
        public string? age_group { get; set; }
        public string? publisher { get; set; }
        public int primary_isbn13 { get; set; }
        public int primary_isbn10 { get; set; }
    }

    public class result
    {
        public string? list_name { get; set; }
        public string? display_name { get; set; }
        public DateOnly bestseller_date { get; set; }
        public DateOnly published_date { get; set; }
        public int rank { get; set; }
        public int rank_last_week { get; set; }
        public int week_on_list { get; set; }
        public int asterisk { get; set; }
        public int dagger { get; set; }
        public string? amazon_product_url { get; set; }
    }


    public class isbns
    {
        public int isbn10 { get; set; }
        public int isbn13 { get; set; }


    }


    public class review
    {

        public string? book_review_link { get; set; }
        public string? first_review_link { get; set; }
        public string? sunday_review_link { get; set; }
        public string? article_chapter_link { get; set; }
    }





}
